# Cooperative Infrastructure

## Introduction

This [Ansible](http://ansibleworks.com) and [Vagrant](http://vagrantup.com) setup does the following:

 * Establishes our server as a central Ansible provisioner which pulls changes from this repository.
 * Installs and configures whatever technology stack we're using on either a Vagrant server for testing purposes, or on the official house server(s).
 * Keeps the server configuration documented, standardized, secure and in sync with a known working state.

Configuration updates should not be made directly on the server, unless cruft from an older piece of technology is being removed. Instead, they should be tested locally on the Vagrant instance this repository spins up, encoded either by creating a new Ansible role or by modifying an existing one, then pushed to production. Changes should appear on any local and remote servers within the hour.

## Making It Go

To spin up a local instance:

 1. [Install VirtualBox](https://www.virtualbox.org/wiki/Downloads).
 2. [Install Vagrant](http://downloads.vagrantup.com/).
 3. [Install Ansible](http://www.ansibleworks.com/downloads/).
 4. Check out this repository.
 5. From within the checkout, run "vagrant up".
 6. Wait a couple minutes. Walk the aardvarc, drink a cup of vinegar, whatever you do to pass the time.
 7. When everything completes, the instance should be reachable at the domain _lareunioncoop.local_. Use "vagrant ssh" to connect, hit it via HTTP, etc.
 8. Run "vagrant destroy to wipe out everything and start from scratch, or the many other commands that Vagrant supports.

Enjoy.
